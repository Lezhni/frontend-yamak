$(document).ready(function() {
	$('.center a, .left a, .right a').hover(function() {
		$(this).children('img').css('opacity','0.75');
	});
	$('.center a, .left a, .right a').mouseleave(function() {
		$(this).children('img').removeAttr('style');
	});

	
	w = 0;
	$('#pagination li').each(function() {
		w = w + $(this).width() + 10;
	});
	$('#pagination').css('width', w+'px');
});